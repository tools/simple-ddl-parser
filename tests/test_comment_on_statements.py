from simple_ddl_parser import DDLParser


def test_comment_on():
    ddl = """
    CREATE schema test;
    CREATE TABLE test.some_table (
        some_column int
    );
    COMMENT ON COLUMN test.some_table.some_column IS 'WOW';
    """

    expected = [
        {"schema_name": "test"},
        {
            "table_name": "some_table",
            "schema": "test",
            "primary_key": [],
            "columns": [
                {
                    "name": "some_column",
                    "type": "int",
                    "size": None,
                    "references": None,
                    "unique": False,
                    "nullable": True,
                    "default": None,
                    "check": None,
                    "comment": "'WOW'",
                }
            ],
            "alter": {},
            "checks": [],
            "index": [],
            "partitioned_by": [],
            "tablespace": None,
        }
    ]

    assert DDLParser(ddl).run() == expected

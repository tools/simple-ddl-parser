from simple_ddl_parser import DDLParser


def test_timestamp_with_time_zone():
    ddl = """
    CREATE TABLE test (
        id int NOT NULL,
        testtime timestamp with time zone NOT NULL
    );
    """

    expected = [
        {
            "alter": {},
            "checks": [],
            "table_name": "test",
            "tablespace": None,
            "primary_key": [],
            "index": [],
            "schema": None,
            "partitioned_by": [],
            "columns": [
                {
                    "name": "id",
                    "type": "int",
                    "size": None,
                    "references": None,
                    "unique": False,
                    "nullable": False,
                    "default": None,
                    "check": None,
                },
                {
                    "name": "testtime",
                    "type": "timestamp with time zone",
                    "size": None,
                    "references": None,
                    "unique": False,
                    "nullable": False,
                    "default": None,
                    "check": None,
                },
            ]
        }
    ]

    assert DDLParser(ddl).run() == expected


def test_column_named_key():
    ddl = """
    CREATE TABLE test (
        id int NOT NULL,
        key int NOT NULL
    );
    """

    expected = [
        {
            "alter": {},
            "checks": [],
            "table_name": "test",
            "tablespace": None,
            "primary_key": [],
            "index": [],
            "schema": None,
            "partitioned_by": [],
            "columns": [
                {
                    "name": "id",
                    "type": "int",
                    "size": None,
                    "references": None,
                    "unique": False,
                    "nullable": False,
                    "default": None,
                    "check": None,
                },
                {
                    "name": "key",
                    "type": "int",
                    "size": None,
                    "references": None,
                    "unique": False,
                    "nullable": False,
                    "default": None,
                    "check": None,
                },
            ]
        }
    ]